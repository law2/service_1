from django.shortcuts import render,HttpResponseRedirect
import requests as rq
from .forms import SongForm
from bs4 import BeautifulSoup
# Create your views here.
def index(request):
    if request.method == 'POST':
        form = SongForm(request.POST)
        q = request.POST['song-search']
        access_token = 'KdqrVbYDLmNkR6SHBHEBggbu98K-gaBxcZb3dpFw7ajkNisRTjgFIqMfHin0ZyQz'
        api_url = 'https://api.genius.com/search?q=' + q + '&access_token=' + access_token
        response = rq.get(api_url).json()

        result = response['response']['hits'][0]['result']
        lyric_url = result['url']
        print(lyric_url)
        res = rq.get(lyric_url)
        raw_lyric = BeautifulSoup(res.content, "html.parser")
        lyric = raw_lyric.find('div', class_="lyrics")
        pretty_lyric = ''
        print(len(lyric))
        return render(request, 'index.html', {'result': lyric})        
    else:
        form = SongForm()
    return render(request, 'index.html', {'form': form, 'result': " "})

def songs(request):
    if request.method == "POST":
        songs = request.POST['song-search']
        detail_json = rq.get("https://api.genius.com/search?q="+songs+"&access_token=KdqrVbYDLmNkR6SHBHEBggbu98K-gaBxcZb3dpFw7ajkNisRTjgFIqMfHin0ZyQz").json()
        HttpResponseRedirect('/')

    else:
        return render(request, 'index.html')